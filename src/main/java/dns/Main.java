package dns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;

import com.sun.net.httpserver.HttpServer;

public class Main {
	
	private static final String NOME = "nome";
	private static final String PORTA = "porta";
	private static final String IP_PAI = "ipPai";
	private static final String PORTA_PAI = "portaPai";
	
	static Endereco dnsPai;
	
	public static void main(String[] args) throws IOException{
		
		validaArgumentos();
		
		HttpServer server = HttpServer.create(new InetSocketAddress(getPorta()), 0);
        server.createContext(RegistraServico.CONTEXTO, new RegistraServico());
        server.createContext(PesquisaNomeServico.CONTEXTO, new PesquisaNomeServico());
        server.createContext(RegistraDNSFilho.CONTEXTO, new RegistraDNSFilho());
        server.start();
        
        System.out.println(String.format("DNS rodando na porta %d", getPorta()));
        
        adicionaDNSPai();
	}

	private static void adicionaDNSPai() {
		String ip = System.getProperty(IP_PAI);
		if(ip == null){
			System.out.println("DNS raiz!!!");
			return;
		}
		
		String porta = System.getProperty(PORTA_PAI);
		
		StringBuilder result = new StringBuilder();
		
		String urlStr = String.format("http://%s:%s/dns-filho?nome=%s&ip=%s&porta=%d", ip, porta, getNome(), "localhost", getPorta());
		
		try{
	    URL url = new URL(urlStr);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	       result.append(line);
	    }
	    rd.close();
	    String response = result.toString();
	    System.out.println(response);
		}
		catch (Exception e) {
			System.out.println("Erro ao registrar em um servidor pai!!");
		}
	}
	
	

	private static void validaArgumentos() {
		if(getNome() == null)
			throw new RuntimeException("Informe o nome do DNS com -Dnome.");
		
		if(System.getProperty(PORTA) == null)
			throw new RuntimeException("Informe a porta com -Dporta.");
		
	}

	private static String getNome() {
		return System.getProperty(NOME);
	}
	
	private static int getPorta() {
		String porta = System.getProperty(PORTA);
		
		return Integer.valueOf(porta);
	}

}
