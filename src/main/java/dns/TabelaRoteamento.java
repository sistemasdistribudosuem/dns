package dns;

import java.util.HashMap;
import java.util.Map;

public class TabelaRoteamento {
	
	private static Map<String, Endereco> tabela = new HashMap<>();
	
	public static void add(String nome, Endereco endereco){
		Endereco registro = tabela.get(nome);
		if(registro != null)
			tabela.remove(nome);
		
		tabela.put(nome, endereco);
	}
	
	public static String pesquisa(String nome){
		Endereco endereco = tabela.get(nome);
		if(endereco != null)
			return String.format("%s:%s", endereco.ip, endereco.porta);
		
		return null;
	}

}
