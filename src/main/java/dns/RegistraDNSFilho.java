package dns;

public class RegistraDNSFilho extends Servico {
	
	static final String CONTEXTO = "/dns-filho";
	
	String nome;
	String ip;
	String porta;
	private String response;

	@Override
	String getContexto() {
		return CONTEXTO;
	}

	@Override
	void setParametro(String chave, String valor) {
		if("nome".equals(chave))
			this.nome = valor;
		
		if("ip".equals(chave))
			this.ip = valor;
				
		if("porta".equals(chave))
			this.porta = valor;
		
	}

	@Override
	int getQuantidadeParametros() {
		return 3;
	}

	@Override
	void executa() {
		if(this.nome == null || this.ip == null || this.porta == null)
			this.response = "Erro ao registrar Servidor DNS.";
		
		System.out.println("registrando DNS Filho  - '"+nome+"' - "+ip+":"+porta);
		TabelaRoteamento.add(nome, new Endereco(ip, porta));
		this.response = String.format("DNS '%s' registrado com sucesso no endereco %s:%s", nome, ip, porta);
	}

	@Override
	String getResponse() {
		return response;
	}

}
