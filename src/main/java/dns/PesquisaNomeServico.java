package dns;

import java.util.ArrayList;
import java.util.Arrays;

public class PesquisaNomeServico extends Servico {

	public static final String CONTEXTO = "/pesquisa";

	String nome;
	String response;
	
	@Override
	String getContexto() {
		return CONTEXTO;
	}

	@Override
	void setParametro(String chave, String valor) {
		if("nome".equals(chave))
			this.nome = valor;
			
	}

	@Override
	int getQuantidadeParametros() {
		return 1;
	}

	@Override
	void executa() {
		String endereco = pesquisaServico();
		
		if(endereco != null){
			this.response = endereco;
			return;
		}
		
		String[] nomes = nome.split("\\.");
		String filho = nomes[nomes.length-1];
		
		
		endereco = TabelaRoteamento.pesquisa(filho);
		
		if(endereco == null){
			this.response = "Endereco nao encontrato";
			return;
		}
		
		String resposta = endereco+","+filho;
		if(nomes.length > 1)
			resposta = resposta+","+getServicoRestante(nomes);
		
		this.response = resposta;
	}
	
	private String getServicoRestante(String[] nomes) {
		ArrayList<String> servicos = new ArrayList<String>(Arrays.asList(nomes));
		servicos.remove(nomes.length - 1);
		
		return String.join(".", servicos);
	}

	private String pesquisaServico(){
		if(nome == null){
			this.response = "Nome nao informado para pesquisa de servico";
			return null;
		}
		
		System.out.println("pesquisando: "+nome);
		return TabelaRoteamento.pesquisa(nome);
	}

	@Override
	String getResponse() {
		return this.response;
	}

	

}
