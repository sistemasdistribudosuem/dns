package dns;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class Servico implements HttpHandler{

	abstract String getContexto();
	
	abstract void setParametro(String chave, String valor);

	abstract int getQuantidadeParametros();
	
	abstract void executa();
	
	abstract String getResponse();
	@Override
	public void handle(HttpExchange args) throws IOException {
		String query = args.getRequestURI().getQuery();
		String[] parametros = query.split("&");
		
		validaQuantidadeParametros(parametros);
		
		for(String param : parametros){
			String[] parametro = param.split("=");
			
			validaChaveValor(parametro);
			
			String chave = parametro[0];
			String valor = parametro[1];
			setParametro(chave, valor);
		}
		
		executa();
		
		addResponse(args);
	}

	private void validaChaveValor(String[] parametro) {
		if(parametro.length != 2)
			throw new RuntimeException("Informe chave e valor no parâmetro (chave=valor)");
	}

	private void validaQuantidadeParametros(String[] parametros) {
		if(parametros.length != getQuantidadeParametros())
			throw new RuntimeException("Informe o nome o ip e a porta do serviço para registrar no DNS. (/"+getContexto()+"?nome=nomeServico&ip=ipServico&porta=portaServico)");
		
	}
	
	void addResponse(HttpExchange args) throws IOException {
		String response = getResponse();
        args.sendResponseHeaders(200, response.length());
        OutputStream os = args.getResponseBody();
        os.write(response.getBytes());
        os.close();
        System.out.println("retornando: "+response);
	}
}
